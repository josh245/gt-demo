# Graphite Demo

Graphite provides a wrapper on top of git which creates "stacked changes".

Rather than a PR that looks like this:

```
really
super
big
```

You end up with multiple PRs that look like this:

```
really
```

and

```
super
```

and

```
big
```

## Why

Smaller MRs are easier to consume but it is painful to wait for a revier to
move forward, or deal with the merge hell that comes from branching off your MR
branch.  Graphite "stacks" your branches and does the work for you.

# New Stuff

First changes.

Second changes.

